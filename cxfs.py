import shutil
import re
import os
import base64

class fsNotMounted(Exception):
	pass

class fsWriteLocked(Exception):
	pass

class fsFileExists(Exception):
	pass

class fsFileNotFound(Exception):
	pass

def isUTF8(data):
	try:
		decoded = data.decode('UTF-8')
	except UnicodeDecodeError:
		return False
	else:
		for ch in decoded:
			if 0xD800 <= ord(ch) <= 0xDFFF:
				return False
		return True

def istext(filename):
	with open(filename, "rb") as f:
		return isUTF8(f.read())

def checkNext(iterator, index):
	currentLine = iterator[index]
	try:
		#Check if next line matches current line
		if iterator[index + 1] != currentLine:
			#If next line is different, return 1
			return 1
		else:
			#If next line is the same, iterate through the list until the index is different
			peek = 2
			try:
				while iterator[index + peek] == currentLine:
					peek += 1
			except IndexError:
				#End of list, return peek value
				return peek
			#No more matching, return peek value
			return peek
	except IndexError:
		#End of list, return 1
		return 1

class fs:
	location="example.cxfs"
	locked = False
	openFS = None
	# Exception if fs is not mounted
	def checkMount(self):
		if self.openFS == None:
			raise fsNotMounted

	# Exception if fs is locked
	def checkLock(self):
		if self.locked:
			raise fsWriteLocked

	# Check if a file exists
	def checkExists(self, file):
		if self.locate(file) == (None, None):
			return False
		else:
			return True

	def findLine(self, list, text):
		lineNumber = 0
		for line in list:
			if line == text:
				return lineNumber
			lineNumber += 1
		return None

	# Mount fs from location
	def mount(self, mode):
		if not os.path.exists(self.location):
			open(self.location, "w+")
		self.openFS = open(self.location, mode)

	# Unmount fs
	def unmount(self):
		self.openFS = None

	# Lock fs
	def lock(self):
		self.checkLock()
		self.locked = True

	# Unlock fs
	def unlock(self):
		self.locked = False

	def backup(self):
		if not os.path.exists(self.location):
			open(self.location, "w+")
		shutil.copyfile(self.location, self.location + ".cxbak")

	def restore(self):
		shutil.copyfile(self.location, self.location + ".cxbak")

	# Locate file, returns beginning and end line numbers
	def locate(self, file):
		self.decompress()
		self.lock()
		self.mount("r")
		lineNumber = 0
		begin = None
		end = None
		#Iterate through lines
		for line in self.openFS.readlines():
			# Check for beginning of file
			if line == "${}\n".format(file):
				begin = lineNumber
			# Check for end of file
			if line == "!{}\n".format(file):
				end = lineNumber
			lineNumber += 1
		self.unmount()
		self.unlock()
		return begin, end

	# Delete file
	def delete(self, file):
		self.decompress()
		self.backup()
		self.lock()
		try:
			self.unlock()
			if self.checkExists(file):
				begin, end = self.locate(file)
				self.mount("r")
				self.lock()
				lines = self.openFS.readlines()
				self.unmount()
				del lines[begin:end+1]
				self.mount("w+")
				self.openFS.writelines(lines)
				self.unmount()
				self.unlock()
				self.compress()
			else:
				self.unlock()
				raise fsFileNotFound
			self.unlock()
		except Exception as e:
			self.unlock()
			self.restore()
			raise e

	def writeFile(self, filename):
		filename = str(filename)
		if not os.path.isfile(filename):
			raise FileNotFoundError(filename)
		if istext(filename):
			with open(filename) as f:
				self.write(os.path.basename(filename), f.read())
		else:
			with open(filename, "rb") as f:
				self.writeBin(os.path.basename(filename), f.read())

	# Write to file
	def write(self, file, content):
		self.decompress()
		self.lock()
		self.backup()
		try:
			self.unlock()
			if self.checkExists(file):
				self.delete(file)
			self.lock()
			self.mount("a")
			contentLines = content.split("\n")
			self.openFS.write("\n${}\n".format(file))
			for line in contentLines:
				self.openFS.write(";" + line + "\n")
			self.openFS.write("!{}\n".format(file))
			self.unmount()
			self.unlock()
		except Exception as e:
			self.unlock()
			self.restore()
			raise e

	def writeBin(self, file, content):
		self.decompress()
		self.lock()
		self.backup()
		try:
			self.unlock()
			if self.checkExists(file):
				self.delete(file)
			self.lock()
			self.mount("a")
			self.openFS.write("\n${}\n".format(file))
			self.openFS.write(";" + base64.b64encode(content).decode("utf-8") + "\n")
			self.openFS.write("!{}\n".format(file))
			self.unmount()
			self.unlock()
		except Exception as e:
			self.unlock()
			self.restore()
			raise e

	def read(self, file):
		self.lock()
		self.unlock()
		self.decompress()
		if not self.checkExists(file):
			raise fsFileNotFound
		begin, end = self.locate(file)
		self.lock()
		self.mount("r")
		lines = self.openFS.readlines()
		self.unlock()
		self.unmount()
		text = ""
		for line in lines[begin+1:end]:
			text += line.split(";")[1]
		text = text.rstrip()
		return text

	def readBin(self, file):
		self.lock()
		self.unlock()
		self.decompress()
		if not self.checkExists(file):
			raise fsFileNotFound
		begin, end = self.locate(file)
		self.lock()
		self.mount("r")
		lines = self.openFS.readlines()
		self.unlock()
		self.unmount()
		text = b""
		for line in lines[begin+1:end]:
			text += base64.b64decode(bytes(line, "utf-8"))
		text = text.rstrip()
		return text

	def listFiles(self):
		self.lock()
		self.mount("r")
		files = []
		fileHeader = re.compile("^\$[^\n]+\n$")
		for line in self.openFS.readlines():
			if fileHeader.fullmatch(line):
				files.append(line.replace("$", "").rstrip())
		self.unlock()
		self.unmount()
		return files

	def writeDir(self, path):
		files = [f for f in os.listdir(path) if os.path.isfile(os.path.join(path, f))]
		for file in files:
			if istext(os.path.join(path, file)):
				self.write(file, open(os.path.join(path, file), "r").read())
			else:
				self.writeBin(file, open(os.path.join(path, file), "rb").read())

	def readToDir(self, path):
		for file in self.listFiles():
			try:
				open(os.path.join(path, file), "wb+").write(self.readBin(file))
			except:
				try:
					open(os.path.join(path, file), "w+").write(self.read(file))
				except:
					print("Error: could not write: " + file)

	def compress(self):
		self.decompress()
		self.lock()
		self.backup()
		try:
			self.mount("r")
			lines = []
			prevLines = []
			fileLines = self.openFS.readlines()
			lookAhead = 1
			for index, line in enumerate(fileLines):
				if lookAhead > 1:
					lines.append("#\n")
					lookAhead -= 1
					continue
				if line in prevLines:
					lookAhead = checkNext(fileLines, index)
					dup = self.findLine(prevLines, line)
					if len("{}@{}\n".format(str(lookAhead) if lookAhead != 1 else "", dup)) < len(line) * lookAhead:
						lines.append("{}@{}\n".format(str(lookAhead) if lookAhead != 1 else "", dup))
					else:
						lines.append(line)
						lookAhead = 1
				else:
					lines.append(line)
				prevLines.append(line)
			lines = list(filter(("#\n").__ne__, lines))
			self.unmount()
			self.mount("w")
			self.openFS.writelines(lines)
			self.unmount()
			self.unlock()
		except Exception as e:
			self.unlock()
			self.unmount()
			self.restore()
			raise e

	def decompress(self):
		self.lock()
		self.backup()
		try:
			self.mount("r")
			lines = []
			rawLines = self.openFS.readlines()
			for line in rawLines:
				reference = re.compile("^[0-9]*\@\d+\n$")
				if reference.fullmatch(line):
					if line[0] == "@":
						replacement = int(line.replace("@", ""))
						repeat = 1
					else:
						repeat = int(line.split("@")[0])
						replacement = int(line.split("@")[1])
					for i in range(repeat):
						lines.append(rawLines[replacement])
				elif line.rstrip() == "":
					pass
				else:
					lines.append(line)
			self.unmount()
			self.mount("w")
			self.openFS.writelines(lines)
			self.unmount()
			self.unlock()
		except Exception as e:
			self.unlock()
			self.unmount()
			self.restore()
			raise e
